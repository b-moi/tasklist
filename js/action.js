var tasks = JSON.parse(localStorage.getItem('tasks'));
if(!tasks){
    tasks = [];
}
/*var t = [
    {
        id: 0,
        deadline: 1555687800,
        title: 'Помыть посуду',
        is_complete: false
    },
    {
        id: 1,
        deadline: 1560225600,
        title: 'Сходить в магазин',
        is_complete: true
    },
    {
        id: 2,
        deadline: 1560236400,
        title: 'Начать праздник',
        is_complete: true
    },
    {
        id: 3,
        deadline: 1560279600,
        title: ' Спеть "чёрный ворон"',
        is_complete: true
    },
];
localStorage.setItem('tasks', JSON.stringify(t));
*/
//http://www.daterangepicker.com/
$('#dates').daterangepicker({
    timePicker: true,
    singleDatePicker: true,
    timePicker24Hour: true,
    locale:{
        format: 'DD.MM.YYYY HH:mm'
    }
});

const container = document.getElementsByClassName('container')[0];
const modalWrapper = document.getElementsByClassName('modal-wrapper')[0];
modalWrapper.addEventListener('click', hideDeleteModal);
document.getElementsByClassName('confirm-modal')[0].addEventListener('click', stopProg);
document.getElementsByClassName('add-modal')[0].addEventListener('click', stopProg);
document.getElementsByClassName('close-modal')[0].addEventListener('click', hideDeleteModal);
document.getElementsByClassName('close-modal')[1].addEventListener('click', hideDeleteModal);
document.getElementsByClassName('add-task')[0].addEventListener('click', showAddModal)
document.getElementById('submit').addEventListener('click', addTask);

const modalButtons = document.getElementsByClassName('item');

for (var i = 0; i < modalButtons.length; i++) {
    modalButtons[i].addEventListener('click', hideDeleteModal);
}

document.getElementById('confirm').addEventListener('click', deleteTask);

var taskId;

renderAll();

const closeButtons = document.getElementsByClassName('task-delete');
for (var i = 0; i < closeButtons.length; i++) {
    closeButtons[i].addEventListener('click', showDeleteModal);
}

function stopProg(event) {
    event.stopPropagation();//для остановки поиска методов у блока родителя
}

function showDeleteModal() {
    taskId = this.id;
    modalWrapper.style.display = 'flex';
    // const modalWrapper=document.getElementsByClassName('modal-wrapper')[0];
    // modalWrapper.style.display='flex';
    document.getElementsByClassName('confirm-modal')[0].style.display="flex";
    document.getElementsByClassName('add-modal')[0].style.display="none";
}
function showAddModal() {
    modalWrapper.style.display = 'flex';
    document.getElementsByClassName('confirm-modal')[0].style.display="none";
    document.getElementsByClassName('add-modal')[0].style.display="flex";
}
function hideDeleteModal() {
    modalWrapper.style.display = 'none';
}

function renderTask(task) {
    container.innerHTML += `
    <div class="task">
                <div class="task-info">
                    <input type="checkbox" ${task.is_complete ? 'checked' : ''}>
                    <span class="task-deadline">${convertToDate(task.deadline)}</span>
                </div>
                <span class="task-name">${task.title}</span>
                <div class="task-delete" id=${task.id}>
                    <i class="fas fa-times"></i>
                </div>
            </div>
    `;
}

function renderAll() {
    container.innerHTML = '';
    tasks.forEach(item => renderTask(item));
}

function convertToDate(time) {
    moment.locale('ru');
    return moment.unix(time).format('lll');
}

function deleteTask() {
    const task = tasks.find(item => item.id === +taskId);
    const index = tasks.indexOf(task);
    tasks.splice(index, 1);
    renderAll();
    localStorage.setItem('tasks', JSON.stringify(tasks));
    const closeButtons = document.getElementsByClassName('task-delete');
    for (var i = 0; i < closeButtons.length; i++) {
        closeButtons[i].addEventListener('click', showDeleteModal);
    }
}

function addTask(){
    var drp = $('#dates').val();
    const unix = moment(drp, 'DD.MM.YYYY HH:mm').unix(); 
    var maxId = 0;   
    for(var i = 0; i < tasks.length; i++){
        if(maxId < tasks[i].id){
            maxId = tasks[i].id;
        }
    }
    const taskName = document.getElementById('title').value;
    const task = {
        id: ++maxId,
        deadline: unix,
        title: taskName,
        is_complete: false
    };
    tasks.push(task);//добавить объект в массив
    sortTasks();//сортировка объектов по дате
    renderAll();//отрисовать карточки
    localStorage.setItem('tasks', JSON.stringify(tasks));
    const closeButtons = document.getElementsByClassName('task-delete');
    for (var i = 0; i < closeButtons.length; i++) {
        closeButtons[i].addEventListener('click', showDeleteModal);
    }
    hideDeleteModal()//закрытие модалки

}

function sortTasks(){
    tasks.sort((a, b) => {
        return a.deadline-b.deadline;
    })
}

